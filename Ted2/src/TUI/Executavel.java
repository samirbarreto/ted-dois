package TUI;

import business.ListaProdutos;

public class Executavel {

	public static void main(String[] args) {

		ListaProdutos lem = new ListaProdutos();
		lem.inserirCategoria(01, "Alimentos");
		lem.inserirCategoria(02, "Produtos de Limpeza");
		lem.inserirCategoria(03, "Papelaria");

		lem.inserirProduto(01, "Feij�o", 5.50, 200, 01);
		lem.inserirProduto(02, "Arroz", 20.0, 200, 01);
		lem.inserirProduto(03, "Carne", 2.0, 2000, 01);

		lem.inserirProduto(01, "Detergente", 0.80, 900, 02);
		lem.inserirProduto(02, "Sab�o", 5.0, 500, 02);

		lem.inserirProduto(01, "Lapis", 0.50, 1000, 03);
		lem.inserirProduto(02, "Borracha", 0.10, 1000, 03);
		lem.inserirProduto(03, "Caneta", 1.50, 1000, 03);

		lem.removerCategoria(04);
		lem.removerCategoria(05);
		lem.removerProduto(03, 02);
		lem.removerProduto(01, 04);
		lem.removerProduto(05, 01);

		lem.imprimirProdutos();

	}

}

package domain;

import business.Categoria;
import business.Produto;

public interface Interface {
	public void imprimir();

	public void imprimirCategoria();

	public void imprimirProduto(int codCat);

	public Categoria inserirCategoria(int codigo, String descricao);

	public Produto inserirProduto(int codCat, int codigo, String descricao);

	public Categoria consultar(int codigo);

	public Produto consultarProduto(int codCat, int codigo);

	public void removerProduto(int codCat, int codigo);

	public void removerCategoria(int codigo);

}

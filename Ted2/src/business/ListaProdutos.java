package business;

public class ListaProdutos {

	private Categoria inicio = null;

	public void inserirCategoria(int codigoCategoria, String descricao) {
		Categoria novo = new Categoria(codigoCategoria, descricao);
		novo.setCodigoCategoria(codigoCategoria);
		novo.setDescricao(descricao);
		if (ehVazia()) {
			inicio = novo;
		} else {
			novo.setProx(inicio);
			inicio = novo;
		}
	}

	public void inserirProduto(int codigoProduto, String descricao, double valor, int quantidade, int codigoCategoria) {
		Produto novo = new Produto(codigoProduto, descricao, valor, quantidade);
		novo.setCodigoProduto(codigoProduto);
		novo.setDescricao(descricao);
		novo.setValor(valor);
		novo.setQuantidade(quantidade);
		Categoria auxC = inicio;
		while (auxC.getProx() != null && auxC.getCodigoCategoria() != codigoCategoria) {
			auxC = auxC.getProx();
		}
		if (auxC.getProx() == null && auxC.getCodigoCategoria() != codigoCategoria) {
			System.out.println("Categoria n�o encontrada");
		} else {
			if (auxC.ehVazia()) {
				auxC.setInicio(novo);
			} else {
				novo.setProx(auxC.getInicio());
				auxC.setInicio(novo);
			}
		}
	}

	public void removerCategoria(int codigoCategoria) {
		if (ehVazia()) {
			System.out.println("Lista de categorias vazia");
		} else {
			Categoria ant = null;
			Categoria aux = inicio;
			while (aux.getProx() != null && aux.getCodigoCategoria() != codigoCategoria) {
				ant = aux;
				aux = aux.getProx();
			}
			if (ant == null) {
				inicio = inicio.getProx();
				aux = null;
			} else if (aux.getProx() == null) {
				if (aux.getCodigoCategoria() == codigoCategoria) {
					ant.setProx(null);
					aux = null;
				} else {
					System.out.println("Categoria n�o encontrada");
				}
			} else {
				ant.setProx(aux.getProx());
				aux = null;
			}
		}
	}

	public void removerProduto(int codigoCategoria, int codigoProduto) {
		if (ehVazia()) {
			System.out.println("Lista de categorias vazia");
		} else {
			Categoria auxC = inicio;
			while (auxC.getProx() != null && auxC.getCodigoCategoria() != codigoCategoria) {
				auxC = auxC.getProx();
			}
			if (auxC.getProx() == null && auxC.getCodigoCategoria() != codigoCategoria) {
				System.out.println("Categoria n�o encontrada");
			} else {
				if (auxC.ehVazia()) {
					System.out.println("N�o possui produtos cadastrados nesta categoria");
				} else {
					Produto ant = null;
					Produto auxP = auxC.getInicio();
					while (auxP.getProx() != null && auxP.getCodigoProduto() != codigoProduto) {
						ant = auxP;
						auxP = auxP.getProx();
					}
					if (ant == null) {
						auxC.setInicio(auxP.getProx());
						auxP = null;
					} else if (auxP.getProx() == null) {
						if (auxP.getCodigoProduto() == codigoProduto) {
							ant.setProx(null);
							auxP = null;
						} else {
							System.out.println("Produto não encontrado");
						}
					} else {
						ant.setProx(auxP.getProx());
						auxP = null;
					}
				}
			}
		}
	}

	public void imprimirCategoria() {
		if (ehVazia()) {
			System.out.println("Lista Vazia");
		} else {
			Categoria aux = inicio;
			while (aux != null) {
				System.out.println(aux.toString());
				aux = aux.getProx();
			}
		}
	}

	public void imprimirProdutoCategoria(int codigoCategoria) {
		if (ehVazia()) {
			System.out.println("Lista de categoria Vazia...");
		} else {
			Categoria auxC = inicio;
			while (auxC.getProx() != null && auxC.getCodigoCategoria() != codigoCategoria) {
				auxC = auxC.getProx();
			}
			if (auxC.getProx() == null && auxC.getCodigoCategoria() != codigoCategoria) {
				System.out.println("Categoria n�o encontrada...");
			} else {
				if (auxC.ehVazia()) {
					System.out.println("N�o h� Produtos nesta categoria...");
				} else {
					Produto auxP = auxC.getInicio();
					while (auxP != null) {
						System.out.println(auxP.toString());
						auxP = auxP.getProx();
					}
				}
			}
		}
	}

	public void imprimirProdutos() {
		if (ehVazia()) {
			System.out.println("Lista de categorias vazia...");
		} else {
			Categoria auxC = inicio;
			while (auxC != null) {
				System.out.println(auxC.toString());
				if (auxC.ehVazia()) {
					System.out.println("N�o h� produtos cadastrados nesta categoria");
				} else {
					Produto auxP = auxC.getInicio();
					while (auxP != null) {
						System.out.println(auxP.toString());
						auxP = auxP.getProx();
					}
				}
				auxC = auxC.getProx();
			}
		}
	}

	public Categoria consultar(int codigo) {
		if (inicio == null) {
			System.out.println("N�o tem elementos na lista!");
		} else {
			Categoria aux = inicio;
			while (aux.getProx() != null && aux.codigo != codigo) {
				aux = aux.getProx();
			}
			if (aux.codigo == codigo) {
				return aux;
			}
		}
		return null;
	}

	public Produto consultarProduto(int codCat, int codigo) {
		if (inicio == null) {
			System.out.println("N�o tem elementos na lista!");
		} else {
			Categoria cat = consultar(codCat);

			if (cat == null) {
				System.out.println("Sem sele��o de categoria!");
			} else {
				Produto aux = cat.getInicio();
				while (aux.getProx() != null && aux.codigo != codigo) {
					aux = aux.getProx();
				}
				if (aux.codigo == codigo) {
					return aux;
				}

			}

		}
		return null;
	}

	public boolean ehVazia() {
		return inicio == null;
	}

}
